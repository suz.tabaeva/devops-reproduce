import sys
import unittest
from unittest import TestLoader, TestSuite
from unittest import TextTestRunner
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select

class SmokeTests(unittest.TestCase):
   def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('/usr/bin/chromedriver',chrome_options=chrome_options)
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        self.target_address = str(sys.argv[1])

   def test_homepage(self):
        """navigate to pet clinic homepage"""
        self.driver.get(self.target_address)
        self.assertTrue('PetClinic' in self.driver.title)


   def tearDown(self):
        self.driver.quit()

class UatTests(unittest.TestCase):
   def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('/usr/bin/chromedriver',chrome_options=chrome_options)
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        self.target_address = str(sys.argv[1])

   def test_add_owner_with_pet_edit_owner(self):
        """test adding of owner with a pet and edit that owner"""
        self.driver.get(self.target_address)
        # navigate to 'Find Owners'
        self.driver.find_element_by_link_text('FIND OWNERS').click()
        # navigate to 'Add owner'
        self.driver.find_element_by_link_text('Add Owner').click()

        # fill in the owner form
        id_list = ['firstName', 'lastName', 'address', 'city', 'telephone']
        keys_list = ['Sherlock', 'Holmes', '221B Baker Street', 'London', '123']
        for (id, key) in zip(id_list, keys_list):
            element = self.driver.find_element_by_id(id)
            element.clear()
            element.send_keys(key)
        # click the 'Add Owner' button
        self.driver.find_element_by_class_name('btn.btn-default').click()

        # verify owner was successfully added
        # name verification
        self.assertEqual(self.driver.find_element_by_xpath('//table/tbody/tr[1]/td').text, keys_list[0] + ' ' + keys_list[1])
        # other attributes
        for i in range(2, len(keys_list)):
            self.assertEqual(self.driver.find_element_by_xpath('//table/tbody/tr['+ str(i) +']/td').text, keys_list[i])

        # test 'add new pet' functionality
        pet_keys = ['Yuki', '2019-03-15', 'dog']
        self.driver.find_element_by_link_text('Add New Pet').click()
        pet_name = self.driver.find_element_by_id('name')
        pet_name.clear()
        pet_name.send_keys(pet_keys[0])
        pet_birthdate = self.driver.find_element_by_id('birthDate')
        pet_birthdate.clear()
        pet_birthdate.send_keys(pet_keys[1])
        pet_select = Select(self.driver.find_element_by_id('type'))
        pet_select.select_by_visible_text(pet_keys[2])
        self.driver.find_element_by_class_name('btn.btn-default').click()
        # verify pet was added successfully
        for i in range(len(pet_keys)):
            self.assertEqual(pet_keys[i], self.driver.find_element_by_xpath('//table/tbody/tr/td/dl/dd['+str(i+1)+']').text)
        #driver.save_screenshot('added_pet.png')

        # test edit owner functionality by editing the telephone number
        self.driver.find_element_by_link_text('Edit Owner').click()
        telephone = self.driver.find_element_by_id('telephone')
        telephone.clear()
        telephone.send_keys('987')
        self.driver.find_element_by_class_name('btn.btn-default').click()
        # verify the owner update
        self.assertEqual(self.driver.find_element_by_xpath('//table/tbody/tr[4]/td').text, '987')
        #driver.save_screenshot('owner_update.png')

   def test_vet_page(self):
        """test if vet page is populated (has at least one record)"""
        # check if veterinary has rows
        self.driver.get(self.target_address+'/vets.html')
        vets_table = self.driver.find_element_by_id('vets')
        rows_vets = vets_table.find_elements_by_tag_name('tr')
        # check vets table is populated (higher than 1 as first is table header row)
        self.assertTrue( len(rows_vets) > 1)

   def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
   loader = TestLoader()
   suite = TestSuite()
   suite.addTests(loader.loadTestsFromTestCase(SmokeTests))
   suite.addTests(loader.loadTestsFromTestCase(UatTests))
   logFile = open(str(sys.argv[2]), 'w')
   runner = TextTestRunner(stream=logFile, descriptions=True, failfast=True, verbosity=2)
   result = runner.run(suite)

   if result.wasSuccessful():
        exit(0)
   else:
        exit(1)